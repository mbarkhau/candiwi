#!/bin/sh

local="$1"
ancestor="$2"
other="$3"
merged="$local".merged

diff3 -m -L LOCAL -L ANCESTOR -L OTHER "$local" "$ancestor" "$other" > "$merged"

rm -f "$local"
mv "$merged" "$local"

exit 0
