# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os, codecs


_dir = os.path.abspath(os.path.dirname(__file__))
MERGE_SCRIPT = os.path.join(_dir, 'merge.sh')
CAN_MERGE_SCRIPT = os.path.join(_dir, 'can_merge.sh')
FILE_MERGED_SCRIPT = os.path.join(_dir, 'file_merged.sh')
MERGE_DIR = '/tmp/candiwi'
DEFAULT_COMMIT_MSG = 'No summary given.'


class MergeStrategy(object):
    
    NONE = 0
    """ Do not merge. """

    CAUTIOUS = 1
    """ Only merge when article edits do not conflict and commit. """
    
    OPTIMISTIC = 2
    """ Only merge when line edits do not conflict and commit. """

    RUTHLESS = 3
    """ Always merge, label conflicts and don't commit if conflicts occured. """


class File(object):

    def __init__(self, title, date, revision, content):
        """ revision: the revision at which the article was last modified """
        self.title = title
        self.content = content
        self.date = date
        self.revision = revision


class Change(object):

    def __init__(self, summary, revision, date, user, parents, diffs):
        
        self.summary = summary
        self.revision = revision
        self.date = date
        self.user = user
        self.parents = parents
        self.diffs = diffs


class Diff(object):

    def __init__(self, parent, child, added, modified, removed, html_diff):

        self.parent = parent
        self.child = child
        self.added = added
        self.removed = removed
        self.modified = modified
        self.html_diff = html_diff


