# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import re
import difflib
import datetime
import subprocess
from mercurial import ui, hg, commands, util, cmdutil, patch
from mercurial import __version__ as _hg_version
from mercurial.node import short, nullid
from mercurial.ancestor import ancestor
from dvcs.common import File, Change, Diff, MergeStrategy
from dvcs.common import MERGE_SCRIPT, MERGE_DIR, DEFAULT_COMMIT_MSG 
from dvcs.common import FILE_MERGED_SCRIPT, CAN_MERGE_SCRIPT
from django.utils.translation import ugettext_lazy as _


class MyDiff(difflib.HtmlDiff):

    def _format_line(self,side,flag,linenum,text):
        try:
            linenum = '%d' % linenum
            id = ' id="%s%s"' % (self._prefix[side],linenum)
        except TypeError:
            id = '' 
        text=text.replace("&","&amp;").replace(">","&gt;").replace("<","&lt;")
        return '<td class="diff_header"%s>%s</td><td>%s</td>' \
               % (id,linenum,text)


def _unicode_str_conversion(func, *args, **kwargs):
    """ input strings unicode -> str 
        output strings str -> unicode 
    """

    def wrapper(*args, **kwargs):

        def to_str(arg):
            if type(arg) == type(u'unicode'):
                return arg.encode('utf-8')
            return arg

        def to_unicode(arg):
            if type(arg) == type('str'):
                return arg.decode('utf-8')
            return arg

        args = map(to_str, args)

        d = {}
        for k, v in kwargs.items():
           d[k] = to_str(v) 
        kwargs.update(d)

        result = func(*args, **kwargs)
        return to_unicode(result)

    wrapper.__name__ = func.__name__
    wrapper.__doc__ = func.__doc__
    wrapper.__dict__.update(func.__dict__)
    return wrapper


htmlDiff = MyDiff()
_short = lambda ctx: short(ctx.node())
_d = lambda data: data.decode('utf-8')
_e = lambda data: data.encode('utf-8')


def _date(ctx):
    return datetime.datetime.fromtimestamp(ctx.date()[0])


def _write_file(path, data):
    try: # no 'with' for 2.5 compatibility :/
        file = None
        file = open(path, 'w')
        file.write(data) 
    finally:
        if file:
            file.close()


# encodeing crap: if shit happens try
# data: in encode, out decode


class Repo(object):
   
    @_unicode_str_conversion
    def __init__(self, repo_path):
        self.repo_path = repo_path
        self._ui = ui.ui()
        self._update_cfg()
        self._repo = hg.repository(self._ui, self.repo_path)

    def _revs(self):
        return map(lambda x: short(x[-1]), self._repo.changelog.index)

    def _default_revision(self, revision):
        if not revision:
            return self._repo.heads()[0]
        return revision

    def _update_cfg(self):
        if _hg_version.version < '1.3':
            self._ui = ui.ui(interactive=False)
            self._ui.updateopts(config=[
                ('ui', 'merge', MERGE_SCRIPT), ('ui', 'quiet', 'True')
            ])
        else: 
            self._ui = ui.ui()
            self._ui.setconfig('ui', 'interactive', 'off')
            self._ui.setconfig('ui', 'quiet', 'True')
            self._ui.setconfig('ui', 'merge', MERGE_SCRIPT)
    
    def _can_merge_cautious(self, rev_a, rev_b):
        ancestor = self._ancestor(rev_a, rev_b)
        diffa = self.diff(ancestor, rev_a)
        diffb = self.diff(ancestor, rev_b)
        filesa = set(diffa.added + diffa.removed + diffa.modified)
        filesb = set(diffb.added + diffb.removed + diffb.modified)
        return not filesa.intersection(filesb)

    def _can_merge_optimistic(self, rev_a, rev_b):
        ancestor = self._ancestor(rev_a, rev_b)
        diffa = self.diff(ancestor, rev_a)
        diffb = self.diff(ancestor, rev_b)
        if (set(diffa.modified).intersection(set(diffb.removed)) or
            set(diffa.removed).intersection(set(diffb.modified))):
            return False
        added = set(diffa.added).intersection(set(diffb.added))
        modified = set(diffa.modified).intersection(set(diffb.modified))
        def unmergable(file):
            return not self._can_merge_file(file, rev_a, ancestor, rev_b)
        return not filter(unmergable, added) + filter(unmergable, modified) 

    def _can_merge_file(self, file, rev_local, rev_ancestor, rev_other):
        if not os.path.exists(MERGE_DIR):
            os.makedirs(MERGE_DIR)
        def write_tmp_file(rev):
            dvcsfile = self.file(file, revision=rev)
            tmpfile = dvcsfile.title.replace('/', '_') + '.' + dvcsfile.revision
            path = os.path.join(MERGE_DIR, tmpfile)
            _write_file(path, dvcsfile.content)
            return path
        files = map(write_tmp_file, [rev_local, rev_ancestor, rev_other])
        can_merge = subprocess.call([CAN_MERGE_SCRIPT] + files) == 0
        map(os.remove, files)
        return can_merge

    def _ancestor(self, revision_a, revision_b):
        def pfunc(node):
            if not node or node == nullid:
                return []
            parents = self._repo.changectx(node).parents()
            return map(lambda p: p.node(), parents)
        return ancestor(revision_a, revision_b, pfunc)

    @_unicode_str_conversion
    def commit(self, msg, username, abs_path=None):
        if abs_path:
            commands.addremove(self._ui, self._repo, abs_path)
        if re.match(r'^\s+$|^$', msg):
            msg = DEFAULT_COMMIT_MSG
        commands.commit(self._ui, self._repo, user=username, message=msg,
                        logfile=False, date=False, addremove=True)

    @_unicode_str_conversion
    def pull(self, path, strategy):
        assert strategy is not MergeStrategy.RUTHLESS
        other = hg.repository(self._ui, path)
        self._repo.pull(remote=other, heads=other.heads(), force=False)
        heads = self.heads()
        merged = True
        if len(heads) == 1:
            self.update_repo(revision=heads[0])
            return
        while len(heads) > 1 and merged:
            merged = self.merge(heads[0], heads[1], strategy)
            heads = self.heads()

    @_unicode_str_conversion
    def merge(self, rev_a, rev_b, strategy):
        if self.can_merge(rev_a, rev_b, strategy):
            self.update_repo(revision=rev_a)
            commands.merge(self._ui, self._repo, node=rev_b)
            if not self.conflicts(): 
                commands.commit(self._ui, self._repo, user='candiwi', 
                                message='merge', logfile=False, date=False, 
                                addremove=True)
                return True
        return False

    @_unicode_str_conversion
    def can_merge(self, rev_a, rev_b, strategy):
        # TODO check if one rev is ancestor of other
        if strategy is MergeStrategy.NONE or self.is_dirty():
            return False
        if strategy is MergeStrategy.CAUTIOUS:
            return  self._can_merge_cautious(rev_a, rev_b)
        if strategy is MergeStrategy.OPTIMISTIC:
            return self._can_merge_optimistic(rev_a, rev_b)
        return True
    
    @_unicode_str_conversion
    def file_exists(self, file_path, revision=None):
        revision = self._default_revision(revision)
        return self._repo.changectx(revision).manifest().has_key(file_path)

    @_unicode_str_conversion
    def file(self, file_path, revision=None):
        revision = self._default_revision(revision)
        fctx = self._repo[revision][file_path]
        ctx = self._repo.changectx(fctx.linkrev()) 
        return File(_d(fctx.path()), _date(ctx), _short(ctx), _d(fctx.data()))

    @_unicode_str_conversion
    def files(self, revision=None):
        revision = self._default_revision(revision)
        paths = self._repo[revision].manifest().keys()
        return map(lambda path: self.file(path, revision=revision), paths)

    @_unicode_str_conversion
    def update_repo(self, revision=None):
        revision = self._default_revision(revision)
        commands.update(self._ui, self._repo, rev=revision, clean=True)

    @_unicode_str_conversion
    def update_file(self, file_path, data, summary, username): 
        abs_file_path = os.path.join(self.repo_path, file_path)
        parent_dir = os.path.dirname(abs_file_path)
        if not os.path.exists(parent_dir):
            os.makedirs(parent_dir)
        _write_file(abs_file_path, data.replace('\r', ''))
        self.commit(summary, username, abs_path=abs_file_path)

    @_unicode_str_conversion
    def delete(self, file_path, summary, username):
        abs_file_path = os.path.join(self.repo_path, file_path)
        os.remove(abs_file_path)
        self.commit(summary, username, abs_path=abs_file_path)

    @_unicode_str_conversion
    def rename(self, old_path, new_path, summary, username):
        abs_old_path = os.path.join(self.repo_path, old_path)
        abs_shortew_path = os.path.join(self.repo_path, new_path)
        commands.rename(self._ui, self._repo, abs_old_path, 
                        abs_shortew_path, force=False, after=False)
        self.commit(summary, username, abs_path=abs_old_path)

    @_unicode_str_conversion
    def diff(self, parent, child='tip'):
        parent = _short(self._repo.changectx(parent))
        child = _short(self._repo.changectx(child))
        modified, added, removed = self._repo.status(parent, child)[:3]
        hd = []
        for file in modified:
            hd.append(htmlDiff.make_table(
                self._repo[parent][file].data().split('\n'),
                self._repo[child][file].data().split('\n'),
                _e(_('Before: ')) + file + '@' + parent, 
                _e(_('After: ')) + file + '@' + child,
                True, 1
            ))
        html_diff = ''.join(hd)
        return Diff(_d(parent), _d(child), map(_d, added), map(_d, modified), 
                    map(_d, removed), _d(html_diff))

    @_unicode_str_conversion
    def change(self, revision=None, make_diffs=True):
        revision = self._default_revision(revision)
        ctx = self._repo.changectx(revision)
        if make_diffs:
            mk_diffs = lambda parent: self.diff(_short(parent), _short(ctx))
            diffs = map(mk_diffs, ctx.parents())
        else:
            diffs = []
        parents = map(_short, ctx.parents())
        return Change(_d(ctx.description()), _short(ctx), _date(ctx), 
                      _d(ctx.user()), parents, diffs)

    @_unicode_str_conversion
    def has_change(self, revision):
        return revision in self._revs()         

    def heads(self):
        return map(short, self._repo.heads())

    @_unicode_str_conversion
    def changes(self, parents=[], children=[], path=None, other=None):
        """ returns changes between parents (exclusive) and children (inclusive)
            path: filter changes affecting a file
            other: filter changes not in other repository
        """
        if not children:
            children = self.heads()
        if not parents:
            parents = [nullid]
        ensure_changeid = lambda c: self._repo[c].node()
        children = map(ensure_changeid, children)
        parents = map(ensure_changeid, parents)
        revs = self._repo.changelog.nodesbetween(parents, children)[0]
        revs = [r for r in revs if r not in parents] # remove parents
        revs = map(short, revs)
        if other: # filter revs not in other repo           
            revs = [r for r in revs if r not in other._revs()]
        if path: # filter revs affecting file               
            revs = filter(lambda r: path in self._repo[r].files(), revs)
        return map(lambda r: self.change(revision=r, make_diffs=False), revs)
    
    @_unicode_str_conversion
    def create_clone(self, dest):
        hg.clone(self._ui, self.repo_path, dest)

    def is_dirty(self):
        return bool(reduce(lambda a, b: a or b, self._repo.status()))

    def conflicts(self):
        def conflict(path):
            abs_path = os.path.join(self.repo_path, path)
            return not subprocess.call([FILE_MERGED_SCRIPT, abs_path]) == 0
        return filter(conflict, self._repo.status()[0])

    @_unicode_str_conversion
    def get_conflict(self, path):
        try: # no 'with' for 2.5 compatibility :/
            file = None
            file = open(os.path.join(self.repo_path, path), 'r')
            return file.read()
        finally:
            if file:
                file.close()

    @_unicode_str_conversion
    def reslove_conflict(self, path, data):
        _write_file(os.path.join(self.repo_path, path), data.replace('\r', ''))


