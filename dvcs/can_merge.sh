#!/bin/sh

local="$1"
ancestor="$2"
other="$3"
merged="$local".merged

diff3 -m -L LOCAL -L ANCESTOR -L OTHER "$local" "$ancestor" "$other" > "$merged"

failed=0
if  grep '^<<<<<<<' "$merged" >/dev/null 2>&1 ||
    grep '^|||||||' "$merged" >/dev/null 2>&1 ||
    grep '^=======' "$merged" >/dev/null 2>&1 ||
    grep '^>>>>>>>' "$merged" >/dev/null 2>&1 ; then
    failed=1
fi

rm -f "$local"
mv "$merged" "$local"

exit "$failed"
