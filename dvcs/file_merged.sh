#!/bin/sh

file="$1"
failed=0

if  grep '^<<<<<<<' "$file" >/dev/null 2>&1 ||
    grep '^|||||||' "$file" >/dev/null 2>&1 ||
    grep '^=======' "$file" >/dev/null 2>&1 ||
    grep '^>>>>>>>' "$file" >/dev/null 2>&1 ; then
    failed=1
fi

exit "$failed"
