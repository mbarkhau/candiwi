# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import re

from django.template import Node
from django.template import Library
from common import community_settings


register = Library()


@register.filter
def localtime(value, timezone):
    return value # TODO
    

@register.simple_tag
def active(request, pattern):
    if re.search(pattern, request.path):
        return 'active'
    return ''


@register.simple_tag
def instance_name():
    name = community_settings.instance_name() 
    if not name:
        name = "candiwi"
    return name


@register.simple_tag
def url_rev(request):
    match = re.match(".*(/at/[0-9a-f]{12,40}).*", request.path)
    if match:
        return match.group(1)
    return ""


