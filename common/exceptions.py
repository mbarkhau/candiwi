# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Daniel Löb <daniel0loeb@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# TODO pass helpfull args to Exception
class AlreadyVoted(Exception): pass
class ArticleAlreadyExists(Exception): pass
class ArticleDoesNotExist(Exception): pass
class DoesNotHaveMainTip(Exception): pass
class IllegalTitle(Exception): pass
class NoMergeConflict(Exception): pass
class MergeConflict(Exception): pass
class SettingsError(Exception): pass
class MultipleHeads(Exception): pass
class NoNewChanges(Exception): pass
class ProposalAlreadyExists(Exception): pass
class RevisionDoesNotExist(Exception): pass
class WrongProposalState(Exception): pass


