# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import ConfigParser
import settings


def _cfg():
    config = ConfigParser.ConfigParser()
    config.read(os.path.join(settings.MAIN_REPO, 'settings'))
    return config


def _getfloat(section, option):
    cfg = _cfg()
    if cfg.has_option(section, option):
        return cfg.getfloat(section, option)
    return None


def _get(section, option):
    cfg = _cfg()
    if cfg.has_option(section, option):
        return cfg.get(section, option)
    return None


def is_valid(cfg):
    has_opt = lambda opt: cfg.has_option('voting', opt)
    return (has_opt('review_duration') and has_opt('minimum_approval') and
            has_opt('minimum_participation') and has_opt('voting_duration'))


def review_duration():
    return _getfloat('voting', 'review_duration')


def minimum_approval():
    return _getfloat('voting', 'minimum_approval')


def minimum_participation():
    return _getfloat('voting', 'minimum_participation')


def voting_duration():
    return _getfloat('voting', 'voting_duration')


def starting_page_article():
    return _get('candiwi', 'starting_page_article')


def instance_name():
    return _get('candiwi', 'instance_name')


