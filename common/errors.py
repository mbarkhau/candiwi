# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Daniel Löb <daniel0loeb@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import traceback
import datetime
import logging
from django.shortcuts import render_to_response
from django.template import RequestContext
from common import community_settings


TITLE, DESCRIPTION = range(2)


# structure = { error_key : ( error_title, error_description) }
_errors = { 
    'prop_not_exist' : ( 
        'Proposal %(title)s does not exist!', 
        'Sorry the requested proposal %(title)s does not exist.' 
    ),

    'wrong_prop_state' : (
        'Wrong proposal state!',
        ('Sorry %(action)s is currently not possible.\n' + 
         'Proposal must be in %(expected)s state.')
    ),

    'rev_not_exist' : (
        'Revision %(rev)s does not exist!',
        'Sorry the specified revision does not exist.'
    ),

    'article_not_exist' : (
        'Article %(article)s does not exist!',
        'Sorry the article %(article)s does not exist.'
    ),

    'already_voted' : (
        'You have already voted on proposal %(title)s!',
        'Sorry you can only vote once on a proposal.'
    ),

    'out_of_date' : (
        'Proposal %(title)s is currently out of date!',
        'Sorry this should never happen.'
    ),

    'multiple_heads' : (
        'Proposal %(title)s has multiple heads!',
        'Sorry all conflicts must first be resolved.'
    ),

    'no_new_changes' : (
        'Proposal %(title)s has no new changes!',
        'Sorry a proposal can only be submitted if it contains new changes.'
    ),

    'merge_conflict' : (
        'Proposal %(title)s is in conflict with other proposals!',
        ('Sorry the proposal %(title)s is in conflict with other proposals ' +
         'that have already been submitted. You will have to wait until the ' +
         'other proposals have been accepted or rejected.'),
    ),
    
    'no_conflit' : (
        'No conflict to resolve in article %(article)s!',
        'Sorry the conflict in article %(article)s has been resolved.'
    ),

    'settings_error' : (
        'Settings file must be valid.',
        ('Sorry, but the settings file you tried to submit is not valid.\n' +
         'You will have to fix this in order to submit successfully.')
    ),
    
    'unknown' : (
        'Unknown error!',
        ('Sorry something bad just happend.\n' +
         'An admin will be notified and look into it as soon as possible.')
    ),
}


def log(message, type='ERROR'):
    logfile = _new_log(type)
    logfile.write(message)
    logfile.close()
    

def log_traceback(type='ERROR'):
    logfile = _new_log(type)
    traceback.print_exc(file=logfile)
    logfile.close()


def _new_log(type):
    """ starts a new log line and returns the log file """
    instance = community_settings.instance_name()
    time = str(datetime.datetime.now())
    errorlog = open("/tmp/candiwi_%s_error.log" % instance,"a")
    errorlog.write("\n# %s TIME: %s\n" % (type, time))
    return errorlog


def render_error_page(request, exception, error_key='unknown', args={}):
    log_traceback() 
    context = { 
        'error_title' : _errors[error_key][TITLE] % args, 
        'error_description' : _errors[error_key][DESCRIPTION] % args
    }
    return render_to_response('error_page.html', context,
                      context_instance=RequestContext(request))
