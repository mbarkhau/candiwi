# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from settings import DVCS
from settings import MAIN_REPO
from proposals.models import Proposal
from common.exceptions import RevisionDoesNotExist


# FIXME this smells i should refactor 
def black_magic(func, *args, **kwargs):
    def wrapper(*args, **kwargs):

        # get proposal and repo
        if kwargs.has_key('proposal') and kwargs['proposal'] is not None:
            kwargs['proposal'] = Proposal.objects.get(title=kwargs['proposal'])
            kwargs['repo'] = DVCS.Repo(kwargs['proposal'].path)
        else:
            kwargs['repo'] = DVCS.Repo(MAIN_REPO)

        # check revision
        if (kwargs.has_key('revision') 
                and kwargs['revision'] is not None 
                and not kwargs['repo'].has_change(kwargs['revision'])):
            raise RevisionDoesNotExist()

        if not kwargs.has_key('article'):
            kwargs['article'] = None
        return func(*args, **kwargs)
    wrapper.__name__ = func.__name__
    wrapper.__doc__ = func.__doc__
    wrapper.__dict__.update(func.__dict__)
    return wrapper


