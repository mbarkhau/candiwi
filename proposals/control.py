# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Daniel Löb <daniel0loeb@googlemail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os, re
import datetime
import ConfigParser
import StringIO
import articles.control as actrl
from django.template.loader import get_template 
from django.template import Context
from settings import DVCS, MAIN_REPO, CLONE_DIR, URL_FULL
from proposals.models import Proposal, Vote
from profiles.models import UserProfile
from dvcs.common import MergeStrategy
from common.exceptions import * 
from common import community_settings
from common import errors


VOTING, REVIEW, EDITING, ACCEPTED, ARCHIVED = Proposal.State.VALUES


def _send_voting_emails(proposal):
    template = get_template('email/voting')
    instance = community_settings.instance_name()
    for profile in UserProfile.objects.filter(voting_mail=True):
        fromaddr = 'noreply@candiwi.org'
        subject = '[%s - candiwi] Voting Notification' % instance
        body = template.render(Context({ 'user' : profile.user, 
                                         'url_full' : URL_FULL, 
                                         'proposal' : proposal }))
        try:
            profile.user.email_user(subject, body, fromaddr)
        except:
            errors.log_traceback()


def _settings_valid(proposal):
    if not proposal.repo.file_exists('settings'):
        return False
    
    article = proposal.repo.file('settings')
    fp = StringIO.StringIO(article.content)
    cfg = ConfigParser.ConfigParser()
    try:
        cfg.readfp(fp)
        return community_settings.is_valid(cfg)
    except:
        return False


def _is_legal_title(title):
    legal = re.match(r'^[^/]+$', title)
    def in_dir():
        abspath = os.path.abspath(os.path.join(CLONE_DIR, title))
        return abspath.startswith(CLONE_DIR)
    return legal and in_dir()


def _can_merge(proposal):
    main_rev = DVCS.Repo(MAIN_REPO).change().revision
    diffm = DVCS.Repo(proposal.path).diff(main_rev)
    filesm = set(diffm.added + diffm.removed + diffm.modified)
    review = list(Proposal.objects.filter(state=REVIEW))
    voting = list(Proposal.objects.filter(state=VOTING))
    for prop in review + voting:
        diffp = DVCS.Repo(prop.path).diff(main_rev)
        filesp = set(diffp.added + diffp.removed + diffp.modified)
        if filesm.intersection(filesp):
            return False
    return True


def can_vote(user, proposal):
    return not bool(proposal.vote_set.filter(user=user).count())


def vote(user, title, accepted):
    """ Exceptions: Proposal.DoesNotExist, WrongProposalState, 
                    AlreadyVoted 
    """
    proposal = Proposal.objects.get(title=title)
    if proposal.state is not VOTING:
        raise WrongProposalState()
    if not can_vote(user, proposal):
        raise AlreadyVoted()
    vote = Vote()
    vote.proposal = proposal
    vote.accepted = accepted
    vote.user = user
    vote.save()


def ls(state=None):
    if state:
        return Proposal.objects.filter(state=state)
    return Proposal.objects.all().order_by('title').order_by('state')


def get(title):
    """ Exceptions: Proposal.DoesNotExist """
    proposal =  Proposal.objects.get(title=title)
    if proposal.repo.is_dirty():
        if proposal.repo.conflicts():
            return proposal 
        else:
            proposal.repo.commit('merge', 'candiwi')
    if len(proposal.repo.heads()) > 1:
        rev_a, rev_b = proposal.repo.heads()[:2]
        proposal.repo.merge(rev_a, rev_b, MergeStrategy.RUTHLESS)
        return get(title)
    return proposal 


def submit(title):
    """ Exceptions: Proposal.DoesNotExist, WrongProposalState, 
                    DoesNotHaveMainTip, MultipleHeads, NoNewChanges, 
                    MergeConflict 
    """
    proposal =  Proposal.objects.get(title=title)
    if proposal.state is not EDITING:
        raise WrongProposalState()
    main_repo = DVCS.Repo(MAIN_REPO)
    main_tip = main_repo.change()
    if not proposal.repo.has_change(main_tip.revision):
        raise DoesNotHaveMainTip()
    if len(proposal.repo.heads()) != 1: 
        raise MultipleHeads()
    if proposal.is_empty:
        raise NoNewChanges()
    if not _can_merge(proposal):
        raise MergeConflict()
    if not _settings_valid(proposal):
        raise SettingsError()
         
    start_review(proposal)
    if not community_settings.review_duration():
        start_voting(proposal)


def start_review(proposal):
    assert proposal.state is EDITING
    proposal.review_start = datetime.datetime.now()
    proposal.state = REVIEW
    proposal.save()


def start_voting(proposal):
    assert proposal.state is REVIEW
    proposal.voting_start = datetime.datetime.now()
    proposal.state = VOTING
    proposal.save()
    _send_voting_emails(proposal)


def accept(proposal):
    assert proposal.state is VOTING
    proposal.save_changes()
    proposal.diff_base_revision = proposal.diff_base_rev()
    proposal.diff_tip_revision = proposal.diff_tip_rev()
    proposal.accepted = datetime.datetime.now()
    proposal.state = ACCEPTED
    proposal.save()


def reject(proposal):
    assert proposal.state is VOTING
    proposal.state = EDITING
    for vote in proposal.vote_set.all():
        vote.delete()
    proposal.save()


def archive(title):
    """ Exceptions: Proposal.DoesNotExist, WrongProposalState """
    proposal =  Proposal.objects.get(title=title)
    if proposal.state is not EDITING:
        raise WrongProposalState()
    proposal.state = ARCHIVED
    proposal.save()


def revive(title):
    """ Exceptions: Proposal.DoesNotExist, WrongProposalState """
    proposal =  Proposal.objects.get(title=title)
    if proposal.state is not ARCHIVED:
        raise WrongProposalState()
    proposal.repo.pull(MAIN_REPO, MergeStrategy.OPTIMISTIC)
    proposal.state = EDITING
    proposal.save()


def create(title):
    """ Exceptions: IllegalTitle, ProposalAlreadyExists """
    if not _is_legal_title(title):
        raise IllegalTitle()
    if len(Proposal.objects.filter(title=title)) > 0:
        raise ProposalAlreadyExists()
    proposal = Proposal()
    proposal.state = EDITING
    proposal.title = title
    proposal.save()
    DVCS.Repo(MAIN_REPO).create_clone(proposal.path)


# In theory, theory and practice are the same. In practice, they're not.
# - Yogi Berra
