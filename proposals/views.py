# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Daniel Löb <daniel0loeb@googlemail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.http import urlquote
from proposals import control, forms
from changes import control as cctrl
from proposals.models import Proposal
from common.errors import render_error_page
from common.exceptions import * 


VOTING, REVIEW, EDITING, ACCEPTED, ARCHIVED = Proposal.State.VALUES


@login_required
def vote(request, title, accepted=True):
    try:
        control.vote(request.user, title, accepted)
        url = '/proposals/view/' + urlquote(title)
        return HttpResponseRedirect(url)

    except Proposal.DoesNotExist, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'voting', 'expected' : 'Voting' }
        return render_error_page(request, ex, 'wrong_prop_state', args)

    except AlreadyVoted, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'already_voted', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def submit(request, title):
    try:
        control.submit(title)
        url = '/proposals/view/' + urlquote(title)
        return HttpResponseRedirect(url)

    except Proposal.DoesNotExist, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'submitting', 'expected' : 'Editing' }
        return render_error_page(request, ex, 'wrong_prop_state', args)

    except DoesNotHaveMainTip, ex: # TODO should not be possible but test it
        args = { 'title' : title }
        return render_error_page(request, ex, 'out_of_date', args)

    except MultipleHeads, ex: 
        args = { 'title' : title }
        return render_error_page(request, ex, 'multiple_heads', args)

    except NoNewChanges, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'no_new_changes', args)

    except MergeConflict, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'merge_conflict', args)

    except SettingsError, ex:
        args = { 'title' : title }
        return render_error_page(request,ex, 'settings_error', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def archive(request, title):
    try:
        control.archive(title)
        url = '/proposals/view/' + urlquote(title)
        return HttpResponseRedirect(url)

    except Proposal.DoesNotExist, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'archiving', 'expected' : 'Editing' }
        return render_error_page(request, ex, 'wrong_prop_state', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def revive(request, title):
    try:
        control.revive(title)
        url = '/proposals/view/' + urlquote(title)
        return HttpResponseRedirect(url)

    except Proposal.DoesNotExist, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'reviving', 'expected' : 'Archived' }
        return render_error_page(request, ex, 'wrong_prop_state', args)

    except Exception, ex:
        return render_error_page(request, ex)


def list(request):
    proposals = control.ls()
    context = { 'proposals' : proposals, 'State' : Proposal.State }
    return render_to_response('proposals/list.html', context,
                              context_instance=RequestContext(request))


@login_required
def create(request):
    try:
        context = {}
        if request.POST:
            form = forms.CreateForm(request.POST)
            if form.is_valid():
                try:
                    title = form.cleaned_data['title'].strip()
                    control.create(title)
                    url = '/proposals/view/' + urlquote(title)
                    return HttpResponseRedirect(url)

                except ProposalAlreadyExists, ex:
                    context['error'] = 'Proposal "%s" already exists!' % title

                except IllegalTitle, ex:
                    context['error'] = '"%s" is an illegal name!' % title
            
        else: # get request
            form = forms.CreateForm()

        context['form'] = form
        return render_to_response('proposals/create.html', context,
                          context_instance=RequestContext(request))

    except Exception, ex:
        return render_error_page(request, ex)


def view(request, title):
    try:
        proposal = control.get(title)
        context = { 'proposal' : proposal, 'State' : Proposal.State, 
                    'can_vote' : False }
        if proposal.state is VOTING and not request.user.is_anonymous():
            context['can_vote'] = control.can_vote(request.user, proposal) 
        return render_to_response('proposals/view.html', context,
                          context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


