# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import datetime
from django.db import models
from django.contrib.auth.models import User
from settings import DVCS, MAIN_REPO, CLONE_DIR
from django.utils.translation import ugettext_lazy as _
from common import community_settings


class Proposal(models.Model):

    class State(object):

        VALUES = tuple(range(5))

        VOTING, REVIEW, EDITING, ACCEPTED, ARCHIVED = VALUES

        CHOICES = ((VOTING,   _("Voting")  ),
                   (REVIEW,   _("Review")  ),
                   (EDITING,  _("Editing") ),
                   (ACCEPTED, _("Accepted")),
                   (ARCHIVED, _("Archived")))

    id = models.AutoField(primary_key=True)

    title = models.CharField(unique=True, max_length=4096)

    state = models.IntegerField(choices=State.CHOICES)

    created = models.DateTimeField(auto_now_add=True)
    
    review_start = models.DateTimeField(null=True)

    voting_start = models.DateTimeField(null=True)

    accepted = models.DateTimeField(null=True)

    diff_base_revision  = models.CharField(max_length=64)
    """ Only needed when proposal is accepted """

    diff_tip_revision  = models.CharField(max_length=64)
    """ Only needed when proposal is accepted """

    def _get_repo(self):
        if not hasattr(self, '_repo'):
            self._repo = DVCS.Repo(self.path)
        return self._repo 

    repo = property(_get_repo)

    def _get_head_revision(self):
        if len(self.repo.heads()) != 1:
            return None
        return self.repo.heads()[0]

    head_revision = property(_get_head_revision)

    def diff_base_rev(self):
        if self.accepted:
            return self.diff_base_revision
        return DVCS.Repo(MAIN_REPO).heads()[0]
    
    def diff_tip_rev(self):
        if self.accepted:
            return self.diff_tip_revision
        return self.repo.heads()[0]

    def _get_diffs(self): # TODO no list of diffs needed for proposal
        if not hasattr(self, '_diffs'): 
            base = self.diff_base_rev()
            tip = self.diff_tip_rev()
            if not base or not tip:
                return []
            self._diffs = [self.repo.diff(base, tip)]
        return self._diffs
            
    diffs = property(_get_diffs)

    def _get_conflicts(self):
        if not hasattr(self, '_conflicts'):
            self._conflicts = self.repo.conflicts()
        return self._conflicts

    conflicts = property(_get_conflicts)

    def save_changes(self):
        """ Save proposal chanegs to DB.
            Only call once when proposal gets Accepted.
        """
        for change in self.changes():
            prop_change = ProposalChange()
            prop_change.revision = change.revision
            prop_change.proposal = self
            prop_change.save()

    def changes(self, *args, **kwargs):
        """ Filter new changes in this proposal
            otherwise same behavior as self.repo.changes
        """
        main_repo = DVCS.Repo(MAIN_REPO)
        if self.accepted:
            prop_changes = ProposalChange.objects.filter(proposal=self)
            if prop_changes:
                main_changes = main_repo.changes(*args, **kwargs)
                prop_revs = map(lambda pc: pc.revision, prop_changes)
                return filter(lambda mc: mc.revision in prop_revs, main_changes)
            else:
                return []
        else:
            kwargs['other'] = main_repo
            return self.repo.changes(*args, **kwargs)

    def _is_empty(self):
        return not self.changes()

    is_empty = property(_is_empty)

    def _get_participation(self):
        assert (self.state in [self.State.ACCEPTED, self.State.VOTING])
        if not hasattr(self, '_participation'):
            if self.state is self.State.ACCEPTED:
                users = User.objects.filter(date_joined__lte=self.accepted)
            else:
                users = User.objects.all()
            if users:
                votes = Vote.objects.filter(proposal=self)
                self._participation = 100.0 * votes.count() / users.count()
            else:
                self._participation = None
        return self._participation

    participation = property(_get_participation)

    def _get_approval(self):
        assert (self.state is self.State.ACCEPTED or 
                self.state is self.State.VOTING)
        if not hasattr(self, '_approval'):
            votes = Vote.objects.filter(proposal=self)
            if votes:
                accepted = Vote.objects.filter(proposal=self, accepted=True)
                self._approval = 100.0 * accepted.count() / votes.count()
            else:
                self._approval = None
        return self._approval

    approval = property(_get_approval)

    def _get_state_time_end(self):
        assert self.state in [self.State.REVIEW, self.State.VOTING]
        if not hasattr(self, '_state_time_end'):
            if self.state is self.State.REVIEW:
                duration = community_settings.review_duration()
                start = self.review_start
            else:
                duration = community_settings.voting_duration()
                start = self.voting_start
            if duration:
                end = start + datetime.timedelta(hours=duration)
                self._state_time_end = end
            else:
                self._state_time_end = None
        return self._state_time_end 
        
    state_time_end = property(_get_state_time_end)

    def _get_state_name(self):
        return self.State.CHOICES[self.state][1]

    state_name = property(_get_state_name)

    def _get_path(self):
        return os.path.join(CLONE_DIR, self.title)

    path = property(_get_path) 

    def __unicode__(self):
        return u"%s (%s)" % (self.title, self.state_name)


class Vote(models.Model):
    
    id = models.AutoField(primary_key=True)

    proposal = models.ForeignKey(Proposal)

    user = models.ForeignKey(User)

    accepted = models.BooleanField()
   
    def __unicode__(self):
        args = (self.user.username, self.proposal.title)
        if self.accepted:
            return u"%s accepted %s" % args
        return u"%s rejected %s" % args

    class Meta:
        
        unique_together = (('proposal', 'user'),)


class ProposalChange(models.Model):

    id = models.AutoField(primary_key=True)

    proposal = models.ForeignKey(Proposal)

    revision  = models.CharField(unique=True, max_length=64, null=True)


