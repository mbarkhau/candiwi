import os

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
DB_FILE = os.path.join(os.path.dirname(PROJECT_DIR), 'sqlite.db')

DATABASE_ENGINE = 'sqlite3'    # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = DB_FILE        # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

URL_PREFIX = ""
# allows for multiple instances to be hosted on the same domain
URL_BASE = "http://localhost:8000"
URL_FULL = URL_BASE + URL_PREFIX
