#!/bin/sh
# setup arch dev environment
workspace="$PWD"

# install dependencies
pacman -S django mercurial --noconfirm
easy_install python-openid
easy_install markdown
easy_install BeautifulSoup

# install Levenshtein from googlecode because original page has disappeared
wget http://pylevenshtein.googlecode.com/files/python-Levenshtein-0.10.1.tar.bz2
tar -xvjf python-Levenshtein-0.10.1.tar.bz2
rm python-Levenshtein-0.10.1.tar.bz2
cd python-Levenshtein-0.10.1 
python setup.py build
python setup.py install
cd "$workspace"
rm -r python-Levenshtein-0.10.1 

# clone from main candiwi repo
hg clone http://bitbucket.org/candiwi/candiwi/

# create instance dirs and repos
mkdir candiwi_main
mkdir candiwi_props
cd "$workspace"/candiwi_main
hg init
cp "$workspace"/candiwi/settings .
hg add settings
hg commit -u candiwi -m "added initial settings"

# init dev sqlite db
cd "$workspace"/candiwi
python manage.py syncdb --noinput

