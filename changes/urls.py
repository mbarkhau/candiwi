# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls.defaults import *


p = r'from/(?P<proposal_title>[^/]+)'
a = r'for/(?P<article_title>.+)'
r = r'(?P<revision>[0-9a-f]{12,40})'


urlpatterns = patterns('changes.views',
    url(r'^list$',                'list', name='main_history'),
    url(r'^%s/list$' % p,         'list', name='proposal_history'),
    url(r'^list/%s$' % a,         'list', name='main_article_history'),
    url(r'^%s/list/%s$' % (p, a), 'list', name='proposal_article_history'),
    url(r'^view/%s$' % r,         'view', name='main_change'),
    url(r'^%s/view/%s$' % (p, r), 'view', name='proposal_change'),
)


