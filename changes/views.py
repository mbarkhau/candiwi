# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render_to_response
from django.template import RequestContext
from proposals.models import Proposal
from changes import control as cctrl
from proposals import control as pctrl
from common.exceptions import *
from common.errors import render_error_page

from settings import DVCS, MAIN_REPO

def list(request, proposal_title=None, article_title=None):
    try:
        changes = cctrl.ls(proposal=proposal_title, article=article_title)
        proposal = None
        if proposal_title:
            proposal = pctrl.get(proposal_title)
            repo = proposal.repo
        else:
            repo = DVCS.Repo(MAIN_REPO)

        article = None
        if article_title:
            article = repo.file(article_title)
        
        context = { 'changes' : changes, 'article' : article, 
                    'proposal' : proposal }
        return render_to_response('changes/list.html', context,
                      context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


def view(request, revision, proposal_title=None):
    try:
        change = cctrl.get(revision=revision, proposal=proposal_title)
        proposal = None
        if proposal_title:
            proposal = pctrl.get(proposal_title)
    
        context = { 'change' : change, 'proposal' : proposal }
        return render_to_response('changes/view.html', context,
                      context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)
    
    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


