# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models
from django.contrib.auth.models import User


class Comment(models.Model):

    id = models.AutoField(primary_key=True)

    text = models.TextField()

    user = models.ForeignKey(User)

    date = models.DateTimeField(auto_now_add=True)


class Commenting(models.Model): 
    """ Association between an Article(section) and a Comment """

    id = models.AutoField(primary_key=True)

    comment = models.ForeignKey(Comment)

    elem_nr = models.IntegerField()
    """
    Which element of the compiled html the comment belongs to

    Supported elements are <p> <h1> <h2> <h3> <span>
    """

    path = models.CharField(max_length=4096)

    changeset = models.CharField(max_length=4096)


