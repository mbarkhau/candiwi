# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import Levenshtein

from BeautifulSoup import BeautifulSoup
from markdown import Markdown

from settings import DVCS, MAIN_REPO, CLONE_DIR

from comments.models import Commenting
from common.errors import log


_d = lambda s: str(s).decode('utf-8')


def copy_comments(old_title, new_title, params):
    """ Associates all comments of a with b (when renaming article)"""
    parent = params['revision']
    tip = params['repo'].change(make_diffs=False).revision
    commentings = (Commenting.objects.filter(path=old_title)
                                     .filter(changeset=parent))

    for c in commentings:
        new_c = Commenting(comment=c.comment,
                           elem_nr=c.elem_nr,
                           path=new_title,
                           changeset=tip)
        new_c.save()


def propagate_comment(commenting, repo_path=None):
    """ Propagates a new comment to subsequent revisions. """
    if not repo_path:
        repo_path = MAIN_REPO
    else:
        repo_path = os.path.join(CLONE_DIR, repo_path)
    repo = DVCS.Repo(os.path.abspath(repo_path))

    parent = repo.change(commenting.changeset, make_diffs=False).revision
    tip = repo.change(make_diffs=False).revision

    if tip == parent:
        # nothing to propagate
        return

    #find subsequent revisions
    path = commenting.path
    revs = repo.changes([parent], [tip], path=path)

    prev_rev = parent
    prev_article = repo.file(path, prev_rev)
    try:
        for cur_rev in revs:
            cur_rev = cur_rev.revision
            cur_article = repo.file(path, cur_rev)
            commenting = _propagate(prev_article, cur_article, cur_rev, [commenting])
            if commenting:
                commenting = commenting[0]
            else:
                break
            prev_rev = cur_rev
            prev_article = cur_article
    except Exception, e:
        print "Exception: ", e


def propagate_comments(title, params):
    """ 
    Propagates comments on an article after a change.
    
    Assumes the title hasn't changed.
    """

    repo = params['repo']
    parent = params['revision']
    tip = repo.change(make_diffs=False).revision
    commentings = (Commenting.objects.filter(path=title)
                                     .filter(changeset=parent))

    article_p = repo.file(title, parent)
    article_t = repo.file(title, tip)
    _propagate(article_p, article_t, tip, commentings)


def _propagate(parent, child, child_rev, commentings):
    """ 
    Propagates the commentings from the parent article to the child
    article if matching elements are found.
    """
    elems_p = _parse_elems(parent)
    elems_c = _parse_elems(child)
    matches = _match_elems(elems_p, elems_c)
    
    #find new elem_nr of commentings
    #create new commenting if there are any
    created = []
    for c in commentings:
        elem_nr = int(c.elem_nr)
        if matches.has_key(elem_nr):
            new_c = Commenting(comment=c.comment,
                               elem_nr=matches[elem_nr],
                               path=c.path,
                               changeset=child_rev)
            new_c.save()
            created.append(new_c)
    return created


def _parse_elems(article):
    html = Markdown().convert(article.content)
    soup = BeautifulSoup(html)
    elems = [_d(s) for s in soup.contents if s and _d(s).strip()]
    return elems


def _match_elems(a, b, min_ratio=0.93):
    """ Returns a map R 
        keys in R are an index I from A
        values in R are a key from B
        such that A[i] == B[r[i]]

        In other words, {1:1, 2:3} means that paragraph 1 is the same in
        A and B whereas paragraph 2 in A is tha same as 3 in B.
    """
    matches = {}
    for k_a in range(len(a)):
        closest_key = None
        closest_ratio = -1
        for k_b in range(len(b)):
            if k_b in matches.values():
                continue
            ratio = Levenshtein.ratio(a[k_a], b[k_b])
            if ratio > closest_ratio:
                closest_key = k_b
                closest_ratio = ratio
                if closest_ratio == 1:
                    break

        if closest_ratio > min_ratio:
            matches[k_a] = closest_key

    return matches


