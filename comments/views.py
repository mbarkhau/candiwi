# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.utils.html import strip_tags

from comments.models import Comment, Commenting
from comments import control


# These are views for RPC calls from the js frontend.
# They return json and do bare minimum validation,
# since that should be handled via js.


def counts(request, changeset, path):
    comments = Commenting.objects.filter(path=path)    \
                                 .filter(changeset=changeset)
    res = {}

    for c in comments:
        if not res.has_key(c.elem_nr):
            res[c.elem_nr] = 0

        res[c.elem_nr] += 1
    
    return HttpResponse(simplejson.dumps(res))


def by_elem(request, elem_nr, changeset, path):

    commentings = (Commenting.objects.filter(path=path)
                                     .filter(changeset=changeset)
                                     .filter(elem_nr=int(elem_nr))
                                     .select_related(depth=1)
                                     .order_by('-comment'))

    metadata = []
    for commenting in commentings:
        c = commenting.comment
        metadata.append({
                          'id'   : c.id, 
                          'text' : c.text, 
                          'user' : c.user.username, 
                          'date' : c.date.ctime()
                        })
    
    return HttpResponse(simplejson.dumps(metadata))


@login_required
def create(request, elem_nr, changeset, path):
    P = request.POST
    if not P.has_key('text') or not P['text']:
        return HttpResponse('{ error: "missing text", success: false }')

    comment = Comment(text      = strip_tags(P['text']),
                      user      = request.user)
    comment.save()

    commenting = Commenting(comment   = comment,
                            elem_nr   = elem_nr,
                            changeset = changeset, 
                            path      = path)
    commenting.save()

    if not P.has_key('proposal') or not P['proposal']:
        control.propagate_comment(commenting)
    else:
        control.propagate_comment(commenting, P['proposal'])

    return HttpResponse('{ success: true }')


@login_required
def delete(request, id):
    """ Not used (or tested) so far """
    comment = Comment.objects.get(id=id)
    if comment.user == request.user:
        comment.delete()
        return HttpResponse('{ success: true }')
    else:
        json = '''
                {
                  success: false, 
                  error: "permission denied"
                }
               '''
        return HttpResponse(json)

