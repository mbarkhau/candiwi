#!/bin/sh
apt-get install mercurial python-django python-openid python-markdown python-levenshtein python-beautifulsoup
hg pull https://bitbucket.org/candiwi/candiwi/
mkdir ../candiwi_main
mkdir ../candiwi_props
cd ../candiwi_main
hg init
cp ../candiwi/settings .
hg add settings
hg commit -m "added settings"
cd ../candiwi
python manage.py syncdb --noinput 
python setup_paths.py
