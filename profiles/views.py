# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django import forms 


class ProfileForm(forms.Form):
    
    first_name = forms.CharField(max_length=30, required=True)

    last_name = forms.CharField(max_length=30, required=True)

    email = forms.EmailField(required=True)

    voting_mail = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ProfileForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        new_email = self.cleaned_data['email']
        if self.user.email != new_email:
            if User.objects.filter(email=new_email).count() > 0:
                raise forms.ValidationError("email address must be unique")
        return new_email


@login_required
def edit(request):
    user = request.user
    profile = user.get_profile()
    P = request.POST
    context = {}
    if P:
        form = ProfileForm(P, user=user)
        if form.is_valid():
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.email = form.cleaned_data['email']
            user.save()
            profile = user.get_profile()
            profile.voting_mail = form.cleaned_data['voting_mail']
            profile.save()
            context['msg'] = "Details Saved!"
    else:
        form = ProfileForm({'first_name': user.first_name,
                            'last_name': user.last_name,
                            'email': user.email,
                            'voting_mail': profile.voting_mail},
                           user=user)

    context['form'] = form
    return render_to_response('profiles/edit.html', context,
                              context_instance=RequestContext(request))


def view(request, username):
    context = {}
    return render_to_response('profiles/view.html', context,
                              context_instance=RequestContext(request))


