# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os, re, codecs, settings
from markdown import Markdown
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.utils.http import urlquote
from django.contrib.auth.decorators import login_required
from proposals.models import Proposal
from proposals import control as pctrl
from articles import control as actrl
from articles import forms
from common.exceptions import * 
from common import community_settings
from common.errors import render_error_page


def starting_page(request):
    try:
        article = community_settings.starting_page_article()
        if article: # TODO check if article exists
            url = '/articles/view/' + urlquote(article)
            return HttpResponseRedirect(url)
        return HttpResponseRedirect('/articles/list')

    except Exception, ex:
        return render_error_page(request, ex)


def list(request, proposal_title=None, revision=None):
    try:
        articles = actrl.ls(proposal=proposal_title, revision=revision)
        proposal = None
        if proposal_title:
            proposal = pctrl.get(proposal_title)

        context = { 'articles' : articles, 'revision' : revision, 
                    'proposal' : proposal, 'State' : Proposal.State }
        return render_to_response('articles/list.html', context,
                      context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


def view(request, article_title, proposal_title=None, revision=None):
    try:
        article = actrl.get(article_title, proposal=proposal_title, 
                            revision=revision)
        if article.title != 'settings':
            article.html = Markdown().convert(article.content)
        else:
            article.html = '<div class="pre">' + article.content + '</div>'

        proposal = None
        if proposal_title:
            proposal = pctrl.get(proposal_title)

        context = { 'article' : article, 'proposal' : proposal, 
                    'revision' : revision, 'State' : Proposal.State }
        return render_to_response('articles/view.html', context,
                                  context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        vars = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', vars)
    
    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)
    
    except ArticleDoesNotExist, ex:
        vars = { 'article' : article_title }
        return render_error_page(request, ex, 'article_not_exist', vars)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def merge(request, article_title, proposal_title):
    try:
        if request.POST:
            form = forms.MergeForm(request.POST)
            if form.is_valid():

                content = form.cleaned_data['content']
                actrl.reslove_conflict(article_title, content, 
                                       proposal=proposal_title)

                url = '/proposals/view/' + urlquote(proposal_title)
                return HttpResponseRedirect(url)

        else: # get request

            content = actrl.get_conflict(article_title, proposal=proposal_title)
            form = forms.MergeForm(initial={ 'content' : content })

        proposal = pctrl.get(proposal_title)
        context = { 'form' : form, 'article' : article_title, 
                    'proposal' : proposal }
        return render_to_response('articles/merge.html', context,
                       context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'editing', 'expected' : 'Editing' }
        return render_error_page(request, ex, 'wrong_prop_state', args)
    
    except ArticleDoesNotExist, ex:
        args = { 'article' : article_title }
        return render_error_page(request, ex, 'article_not_exist', args)
    
    except NoMergeConflict, ex:
        args = { 'article' : article_title }
        return render_error_page(request, ex, 'no_conflit', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def edit(request, article_title, proposal_title, revision):
    try:
        context = {}
        proposal = pctrl.get(proposal_title)
        if proposal.state != proposal.State.EDITING:
            raise WrongProposalState()
        article = actrl.get(article_title, proposal=proposal_title, 
                            revision=revision)

        if request.POST:
            form = forms.EditForm(request.POST)
            if form.is_valid():
     
                content = form.cleaned_data['content']
                summary = form.cleaned_data['summary']
                username = request.user.username
                actrl.update(article_title, content, summary, username, 
                             proposal=proposal_title, revision=revision)

                url_args = (urlquote(proposal_title), urlquote(article_title))
                url = '/articles/from/%s/view/%s' % url_args
                return HttpResponseRedirect(url)

        else: # get request

            form = forms.EditForm(initial={ 'content' : article.content })

        context['article'] = article
        context['commit_revision'] = revision
        context['proposal'] = proposal
        context['form'] = form
        return render_to_response('articles/edit.html', context,
                                   context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)
    
    except WrongProposalState, ex:
        args = { 'action' : 'editing', 'expected' : 'Editing' }
        return render_error_page(request, ex, 'wrong_prop_state', args)
    
    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)
    
    except ArticleDoesNotExist, ex:
        args = { 'article' : article_title }
        return render_error_page(request, ex, 'article_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def delete(request, article_title, proposal_title, revision):
    try:
        context = {}
        proposal = pctrl.get(proposal_title)
        if proposal.state != proposal.State.EDITING:
            raise WrongProposalState()
        article = actrl.get(article_title, proposal=proposal_title, 
                            revision=revision)

        if request.POST:
            form = forms.DeleteForm(request.POST)
            if form.is_valid():

                summary = form.cleaned_data['summary']
                username = request.user.username
                actrl.rm(article_title, summary, username, 
                         proposal=proposal_title, revision=revision)

                url = '/proposals/view/' + urlquote(proposal_title)
                return HttpResponseRedirect(url)

        else: # get request
            form = forms.DeleteForm()

        context['proposal'] = proposal
        context['commit_revision'] = revision
        context['article'] = article 
        context['form'] = form
        return render_to_response('articles/delete.html', context,
                          context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'deleting', 'expected' : 'Editing' }
        return render_error_page(request, ex, 'wrong_prop_state', args)
    
    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)
    
    except ArticleDoesNotExist, ex:
        args = { 'article' : article_title }
        return render_error_page(request, ex, 'article_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def rename(request, article_title, proposal_title, revision):
    try:
        context = {}
        proposal = pctrl.get(proposal_title)
        if proposal.state != proposal.State.EDITING:
            raise WrongProposalState()

        if request.POST:
            form = forms.RenameForm(request.POST)
            if form.is_valid():

                try:
                    new_title = form.cleaned_data['new_title'].strip()
                    summary = form.cleaned_data['summary']
                    username = request.user.username
                    actrl.mv(article_title, new_title, summary, username, 
                             proposal=proposal_title, revision=revision)

                    url_args = (urlquote(proposal_title), urlquote(new_title))
                    url = '/articles/from/%s/view/%s' % url_args
                    return HttpResponseRedirect(url)
                
                except ArticleAlreadyExists, ex: # new name
                    msg = 'Article "%s" already exists!'
                    context['error'] = msg % new_title

                except IllegalTitle, ex:
                    msg = '"%s" is an illegal name!'
                    context['error'] = msg % new_title
            
        else: # get request
            form = forms.RenameForm()

        article = actrl.get(article_title, proposal=proposal_title, 
                            revision=revision)
        context['commit_revision'] = revision
        context['article'] = article 
        context['proposal'] = proposal 
        context['form'] = form
        return render_to_response('articles/rename.html', context,
                          context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)
    
    except WrongProposalState, ex:
        args = { 'action' : 'renaming', 'expected' : 'Voting' }
        return render_error_page(request, ex, 'wrong_prop_state', args)
    
    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)
    
    except ArticleDoesNotExist, ex: # old name
        args = { 'article' : article_title }
        return render_error_page(request, ex, 'article_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


@login_required
def create(request, proposal_title, revision):
    try:
        proposal = pctrl.get(proposal_title)
        if proposal.state != proposal.State.EDITING:
            raise WrongProposalState()
        context = { 'proposal' : proposal, 'commit_revision' : revision }

        if request.POST:
            form = forms.CreateForm(request.POST)
            if form.is_valid():

                try:
                    title = form.cleaned_data['title'].strip()
                    content = form.cleaned_data['content']
                    summary = form.cleaned_data['summary']
                    username = request.user.username
                    actrl.create(title, content, summary, username, 
                                 proposal=proposal_title, revision=revision)

                    url_args = (urlquote(proposal_title), urlquote(title))
                    url = '/articles/from/%s/view/%s' % url_args
                    return HttpResponseRedirect(url)
                
                except ArticleAlreadyExists, ex:
                    context['error'] = 'Article "%s" already exists!' % title
                
                except IllegalTitle, ex:
                    context['error'] = '"%s" is an illegal name!' % title

        else: # get request
            form = forms.CreateForm()

        context['form'] = form
        return render_to_response('articles/create.html', context,
                                   context_instance=RequestContext(request))

    except Proposal.DoesNotExist, ex:
        args = { 'title' : proposal_title }
        return render_error_page(request, ex, 'prop_not_exist', args)

    except WrongProposalState, ex:
        args = { 'action' : 'editing', 'expected' : 'Editing' }
        return render_error_page(request, ex, 'wrong_prop_state', args)
    
    except RevisionDoesNotExist, ex:
        args = { 'rev' : revision }
        return render_error_page(request, ex, 'rev_not_exist', args)

    except Exception, ex:
        return render_error_page(request, ex)


