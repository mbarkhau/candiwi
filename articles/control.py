# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os, re

from settings import DVCS
from settings import MAIN_REPO, CLONE_DIR
from proposals.models import Proposal
from dvcs.common import MergeStrategy
from common.exceptions import *
from common.decorators import black_magic
import comments.control as c_ctrl



VOTING, REVIEW, EDITING, ACCEPTED, ARCHIVED = Proposal.State.VALUES


def _editing_boiler_plate(func, *args, **kwargs):
    def wrapper(*args, **kwargs):
        # proposal, revision must be explicit and state must be editing
        assert kwargs['proposal'] is not None
        assert kwargs['revision'] is not None
        if kwargs['proposal'].state is not EDITING:
            raise WrongProposalState()
        kwargs['repo'].update_repo(revision=kwargs['revision'])
        return func(*args, **kwargs)
    wrapper.__name__ = func.__name__
    wrapper.__doc__ = func.__doc__
    wrapper.__dict__.update(func.__dict__)
    return wrapper


def _is_legal_title(title, proposal, revision):
    legal = not re.match(r'^/.*|.*/$', title)
    def in_dir():
        abspath = os.path.abspath(os.path.join(proposal.path, title))
        return abspath.startswith(proposal.path)
    dir_conflict = lambda a: re.match(r'^%s/.*' % a.title, title)
    def dir_conflicts():
        articles = proposal.repo.files(revision=revision)
        return filter(dir_conflict, articles)
    return legal and in_dir() and not dir_conflicts()


def _clean(content):
    if not content.endswith('\n'):
        content += '\n'
    return content


@black_magic
def ls(proposal=None, **kwargs):
    """ Black Magic kwargs: proposal, revision 
        Exceptions: RevisionDoesNotExist, Proposal.DoesNotExist
    """
    articles = kwargs['repo'].files(revision=kwargs['revision'])
    return sorted(articles, lambda a, b: cmp(a.title, b.title))


@black_magic
def get(title, **kwargs):
    """ Black Magic kwargs: proposal, revision 
        Exceptions: ArticleDoesNotExist, RevisionDoesNotExist, 
                    Proposal.DoesNotExist
    """
    if not kwargs['repo'].file_exists(title, revision=kwargs['revision']):
        raise ArticleDoesNotExist()
    return kwargs['repo'].file(title, revision=kwargs['revision'])


@black_magic
def get_conflict(title, **kwargs):
    """ Black Magic kwargs: proposal 
        Exceptions: Proposal.DoesNotExist, WrongProposalState, 
                    ArticleDoesNotExist, NoMergeConflict
    """
    proposal = kwargs['proposal']
    if proposal.state is not EDITING:
        raise WrongProposalState()
    if not proposal.repo.file_exists(title):
        raise ArticleDoesNotExist()
    if not title in proposal.repo.conflicts():
        raise NoMergeConflict()
    return proposal.repo.get_conflict(title)


@black_magic
def reslove_conflict(title, content, **kwargs):
    """ Black Magic kwargs: proposal 
        Exceptions: Proposal.DoesNotExist, WrongProposalState, 
                    ArticleDoesNotExist, NoMergeConflict
    """
    proposal = kwargs['proposal']
    if proposal.state is not EDITING:
        raise WrongProposalState()
    if not proposal.repo.file_exists(title):
        raise ArticleDoesNotExist()
    if not title in proposal.repo.conflicts():
        raise NoMergeConflict()
    proposal.repo.reslove_conflict(title, _clean(content))
    if len(proposal.repo.conflicts()) == 0:
        proposal.repo.commit('manual merge', 'candiwi')


@black_magic
@_editing_boiler_plate
def update(title, content, summary, username, **kwargs):
    """ Black Magic kwargs: proposal, revision 
        Exceptions: Proposal.DoesNotExist, RevisionDoesNotExist, 
                    WrongProposalState, ArticleDoesNotExist    
    """
    if not kwargs['repo'].file_exists(title, revision=kwargs['revision']):
        raise ArticleDoesNotExist()
    kwargs['repo'].update_file(title, _clean(content), summary, username)
    c_ctrl.propagate_comments(title, kwargs)


@black_magic
@_editing_boiler_plate
def mv(old_title, new_title, summary, username, **kwargs):
    """ Black Magic kwargs: proposal, revision 
        Exceptions: Proposal.DoesNotExist, RevisionDoesNotExist, 
                    WrongProposalState, ArticleDoesNotExist, 
                    ArticleAlreadyExists, IllegalTitle
    """
    if not kwargs['repo'].file_exists(old_title, revision=kwargs['revision']):
        raise ArticleDoesNotExist()
    if kwargs['repo'].file_exists(new_title, revision=kwargs['revision']):
        raise ArticleAlreadyExists()
    if not _is_legal_title(new_title, kwargs['proposal'], kwargs['revision']):
        raise IllegalTitle()
    kwargs['repo'].rename(old_title, new_title, summary, username)
    c_ctrl.copy_comments(old_title, new_title, kwargs)


@black_magic
@_editing_boiler_plate
def rm(title, summary, username, **kwargs):
    """ Black Magic kwargs: proposal, revision 
        Exceptions: Proposal.DoesNotExist, RevisionDoesNotExist, 
                    WrongProposalState, ArticleDoesNotExist
    """
    if not kwargs['repo'].file_exists(title, revision=kwargs['revision']):
        raise ArticleDoesNotExist()
    kwargs['repo'].delete(title, summary, username)


@black_magic
@_editing_boiler_plate
def create(title, content, summary, username, **kwargs):
    """ Black Magic kwargs: proposal, revision 
        Exceptions: Proposal.DoesNotExist, RevisionDoesNotExist, 
                    WrongProposalState, ArticleAlreadyExists, IllegalTitle
    """
    if not _is_legal_title(title, kwargs['proposal'], kwargs['revision']):
        raise IllegalTitle()
    if kwargs['repo'].file_exists(title, revision=kwargs['revision']):
        raise ArticleAlreadyExists()
    kwargs['repo'].update_file(title, _clean(content), summary, username)


