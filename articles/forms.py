# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.forms import Form, CharField, ChoiceField, TypedChoiceField
from django.forms.widgets import Textarea, TextInput, Select


textarea = { 'cols' : '90', 'rows' : '20' }
textinput = { 'size' : '40' }
select = {}


class MergeForm(Form):
    
    content = CharField(widget=Textarea(attrs=textarea), 
                        label="Content", required=True)


class EditForm(Form):
    
    content = CharField(widget=Textarea(attrs=textarea), 
                        label="Content", required=True)

    summary = CharField(widget=TextInput(attrs=textinput),
                        label="Summary", required=False)


class CreateForm(Form):
    
    title = CharField(widget=TextInput(attrs=textinput),
                        label="Title", required=True)
    
    content = CharField(widget=Textarea(attrs=textarea), 
                        label="Content", required=True)
    
    summary = CharField(widget=TextInput(attrs=textinput),
                        label="Summary", required=False)


class DeleteForm(Form):

    summary = CharField(widget=TextInput(attrs=textinput),
                        label="Summary", required=False)


class RenameForm(Form):

    new_title = CharField(widget=TextInput(attrs=textinput),
                        label="New Title", required=True)

    summary = CharField(widget=TextInput(attrs=textinput),
                        label="Summary", required=False)


