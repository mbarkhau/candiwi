# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls.defaults import *


p = r'from/(?P<proposal_title>[^/]+)'
r = r'at/(?P<revision>[0-9a-f]{12,40})'
a = r'(?P<article_title>.+)'
pra = (p, r, a)


urlpatterns = patterns('articles.views',
    
    url(r'^list$',                'list', name='main_articles'),
    url(r'^%s/list$' % p,         'list', name='proposal_articles'),
    url(r'^%s/list$' % r,         'list', name='main_articles_at_revision'),
    url(r'^%s/%s/list$' % (p, r), 'list', name='proposal_articles_at_revision'),

    url(r'^view/%s$' % a,         'view', name='main_article'),
    url(r'^%s/view/%s$' % (r, a), 'view', name='main_article_at_revision'),
    url(r'^%s/view/%s$' % (p, a), 'view', name='proposal_article'),
    url(r'^%s/%s/view/%s$' % pra, 'view', name='proposal_article_at_revision'),
   
    # when editing proposal and revision must be given explicitly! 
    url(r'^%s/%s/create$' % (p, r), 'create', name='create_article'),
    url(r'^%s/%s/delete/%s$' % pra, 'delete', name='delete_article'),
    url(r'^%s/%s/rename/%s$' % pra, 'rename', name='rename_article'),
    url(r'^%s/%s/edit/%s$' % pra,   'edit',   name='edit_article'),

    url(r'^%s/merge/%s$' % (p, a),  'merge',  name='resolve_conflict'),
)


