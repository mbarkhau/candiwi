var _commentElems = [];
var _curElem = 0;
var _commentBox;

var registerForHighlight = function(text, cmt) {
    if (text.indexOf(">") >= 0 || text.indexOf("&gt;") >= 0) {
        cmt.hover(highlight, unHighlight);
    }
};

var highlight = function(evt) {
    var text = $(".text", this).attr("innerHTML");
    var matches = text.match(/&gt;(.+)\n?/g);
    for (i in matches) {
        matches[i] = matches[i].replace("&gt; ", "");
        matches[i] = matches[i].replace("&gt;", "");
        matches[i] = matches[i].replace("\n", "");
        $(_commentElems[_curElem]).parent().highlight(matches[i]);
    }
};

var unHighlight = function(evt) {
    $(_commentElems[_curElem]).parent().removeHighlight();
};

var getTmpl = function(id) { return document.getElementById(id).innerHTML; }

var makeComment = function(data) {
    var dateStr = tmpl(getTmpl("date-tmpl"), { d : new Date(data.date) });
    var cmt = $(tmpl(getTmpl("comment-tmpl"), { data: data, dateStr: dateStr }));
    registerForHighlight(data.text, cmt);
    return cmt;
};

var updateBlockCommentsTab = function(elem_nr) {
    _curElem = elem_nr;
    var cmtBox = $("#block-comments");
    cmtBox.empty();
    var getCommentsCallback = function(data) {
        for (i in data) {
            cmtBox.append(makeComment(data[i]));
        }
    };
    var url = "/comments/by_elem/" + elem_nr + "/" + _changeset + "/" + _article_path;
    $.getJSON(url, getCommentsCallback);
};

var submitComment = function(evt) {
    var text = $("#new-comment textarea");
    var url = "/comments/create/" + _curElem + "/" +
                _changeset + "/" + _article_path;

    var params = { 'text' : text.attr("value")};
    if (typeof(_proposal) != "undefined") {
        params['proposal'] = _proposal;
    }
    $.post(url, params);
    text.attr("disabled", true);
    var btn = $(this);
    btn.attr("disabled", true);
    btn.unbind("click");
    var ack = $("<div class='comment-ack'>" +
                "Comment submitted!</div>");
    btn.after(ack);
    ack.fadeIn();

    var cnt = parseInt(_commentElems[_curElem].innerHTML);
    _commentElems[_curElem].innerHTML = cnt + 1;
    return false;
};

var initCommentBox = function() {
    _commentBox = $("#comment-box");
    var opts = { 
        autoOpen: false,
        height: 300,
        width:400, 
        minWidth:350,
        minHeight:300,
        position:['right','top']
    };
    _commentBox.dialog(opts);
    _commentBox.tabs();

    var cmtTab = $("#new-comment");
    var cmtText = $("textarea", cmtTab);
    var cmtSubmit = $("input", cmtTab);

    if (_username === "") {
        cmtText.attr("disabled", true);
        cmtSubmit.css("display", "none");
        cmtText.attr("value", "Login required!");
    } else {
        cmtText.attr("value", "");
        cmtText.attr("disabled", false);
        $(".comment-ack", _commentBox).remove();
        cmtSubmit.attr("disabled", false);
        cmtSubmit.click(submitComment); 
    }
};

var tabSelectDispatcher = function(evt, ui) {
    if (ui.index != undefined && ui.index === 1) {
        initCommentBox();
    } else {
        updateBlockCommentsTab(_curElem);
    }
}

var viewCommentsCallback = function(evt) {
    // display the comment box
    if (!_commentBox.dialog('isOpen')) {
        _commentBox.dialog('open');
    }

    // reset the selection highlight for the paragraph
    var content = $("#content");
    content.children("*").removeClass("selected");
    
    var elem_nr = _commentElems.indexOf(this);
    $(this).parent().addClass("selected");
    _commentBox.tabs('select', 0);
    _commentBox.bind('tabsselect', tabSelectDispatcher);
    updateBlockCommentsTab(elem_nr);
};

// set up the comment bubbles and handlers
var initCommentCounts = function() {
    var content = $("#content");
    
    var commentButton = '<span class="comment-btn">0</span>';
    content.children("*").prepend(commentButton);
    var registerElems = function(i, elem) {
        _commentElems[i] = elem;
    };
    var buttons = $(".comment-btn");
    buttons.each(registerElems);
    buttons.click(viewCommentsCallback);

    var url = "/comments/counts/" + _changeset + "/" + _article_path;
    var getCountsCallback = function(data) {
        for (i in data) {
            _commentElems[i].innerHTML = data[i];
            if (data[i] > 0){
                $(_commentElems[i]).css("background-image", "url(/static_media/comment.png)");
            }
        }
    };
    $.getJSON(url, getCountsCallback);
};

$(document).ready(initCommentBox);
$(document).ready(initCommentCounts);
