/* Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
 * Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
 * Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var providers = {

    openid: {
        name: 'OpenID',
        url: null
    },

    google: {
        name: 'Google',
        url: 'https://www.google.com/accounts/o8/id'
    },

    yahoo: {
        name: 'Yahoo',
        url: 'http://yahoo.com/'
    },

    aol: {
        name: 'AOL',
        label: 'Enter your AOL screenname.',
        url: 'http://openid.aol.com/{username}'
    },

    myopenid: {
        name: 'MyOpenID',
        label: 'Enter your MyOpenID username.',
        url: 'http://{username}.myopenid.com/'
    },

    livejournal: {
        name: 'LiveJournal',
        label: 'Enter your Livejournal username.',
        url: 'http://{username}.livejournal.com/'
    },

    flickr: {
        name: 'Flickr',
        label: 'Enter your Flickr username.',
        url: 'http://flickr.com/{username}/'
    },

    technorati: {
        name: 'Technorati',
        label: 'Enter your Technorati username.',
        url: 'http://technorati.com/people/technorati/{username}/'
    },

    wordpress: {
        name: 'Wordpress',
        label: 'Enter your Wordpress.com username.',
        url: 'http://{username}.wordpress.com/'
    },

    blogger: {
        name: 'Blogger',
        label: 'Your Blogger account',
        url: 'http://{username}.blogspot.com/'
    },

    verisign: {
        name: 'Verisign',
        label: 'Your Verisign username',
        url: 'http://{username}.pip.verisignlabs.com/'
    },

    vidoop: {
        name: 'Vidoop',
        label: 'Your Vidoop username',
        url: 'http://{username}.myvidoop.com/'
    },

    claimid: {
        name: 'ClaimID',
        label: 'Your ClaimID username',
        url: 'http://openid.claimid.com/{username}'
    }
};

var selected_provider;

$(document).ready(function() {

    var x = $('#openid_shortcuts');
    $('#openid_shortcuts').css("visibility",'visible');

    $('#openid_shortcuts > img').click(function(ev){
        $('#openid_userid').css("visibility",'visible');
        $('#openid_label').attr('innerHTML',"Enter your username for "+providers[this.id].name);
        selected_provider = this.id;
    });

    $('#openid_signin').click(function(ev){
        p = providers[selected_provider];
        user = $('#openid_user').attr('value');
        $('#id_openid_identifier').val( p.url.replace("{username}", user) );
        $('#openid_form').submit();
    });

    $('#google').click(function(ev){
        p = providers['google'];
        $('#id_openid_identifier').val( p.url );
        $('#openid_form').submit();
    });

    $('#yahoo').click(function(ev){
        p = providers['yahoo'];
        $('#id_openid_identifier').val( p.url );
        $('#openid_form').submit();
    });

    $('#aol').click(function(ev){
        $('#openid_userid').css("visibility",'visible');
        $('#openid_label').attr('innerHTML',"Enter your username for "+providers['aol'].name);
        selected_provider = 'aol';
    });
 });

