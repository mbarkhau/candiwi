��    R      �  m   <      �  �   �  �   �  �   1  �   �  S   Z	  L   �	  7   �	  �   3
  �  �
  ?   �  �   �  Y   �  l   �  �   c  P   �  Q   6  B   �  P   �  l        �     �     �     �     �     �     �     �     �     �     �     �     �     �       7   !     Y     o     �     �  &   �     �     �  7   �     �                    '     .  '   A     i     o     v          �     �     �     �     �  	   �     �     �  	   �     �     �                              #     )     0     8     G  	   M     W     ^     j     |     �  5  �  �   �  �   p  �     �   �  U   "  M   x  <   �  �     �  �  T   �  �   �  ^   �  m   �  �   b  U   (  W   ~  I   �  [       j   |      �   
   �      �   	   !  
   !      !  
   ,!     7!     ?!     M!     ^!     e!  	   n!     x!     �!  >   �!     �!     �!     "     "  (   "  
   :"     E"  ?   Q"     �"     �"     �"     �"  	   �"     �"  '   �"  	   #  	   #  	   #     ##     1#     C#     K#     \#     i#     v#     #     �#  	   �#     �#     �#  
   �#     �#     �#  	   �#  	   �#     �#  
   �#     �#     $     $     $  
   *$     5$     K$     e$     l$        A          J   I   E             /   (          =   
          5               "             <   8   #          3   $   4      C   7   0   ,       9      :      6         P   G   >   M       @             ?             +           &   B   %   L                K           -                 D         !   	                  )      H               .   N   1       R   O   ;   '          F   2   Q   *    
                    <a href="/changes/from/%(url_title)s/view/%(url_rev)s">
                        %(date)s change %(rev)s made by %(user)s
                    </a>
                 
                    If you'd like to register, or if you've already
                    registered, enter your OpenID
                 
                %(article_title)s from proposal  
                <a href="/proposals/view/%(url_prop_title)s">%(prop_title)s</a>
             
                <a href="/changes/view/%(url_rev)s">
                    %(date)s change %(rev)s made by %(user)s
                </a>
                 
                History of article <b>%(article_title)s</b> in Trunk.
             
                History of proposal <b>%(proposal_title)s</b>.
             
                Last changed on %(date)s.
             
                This proposal has multiple head revisions! Article
                conflicts must first be resolved bofore further
                editing can take place.
             
            <p><b>Info OpenID</b><br/>
            OpenID eliminates the need for multiple usernames across different
            websites, simplifying your online experience.
            </p>
            <p>You get to choose the OpenID Provider that best meets your
            needs and most importantly that you trust. At the same time,
            your OpenID can stay with you, no matter which Provider you
            move to. And best of all, the OpenID technology is not
            proprietary and is completely free.
            </p>

            <p>You can use OpenIDs from any provider. If you don't have
            already an OpenID you can find a provider in this 
            <a href="http://openid.net/get/">provider list</a>.</p>

            <p>To create an OpenID for a test user, you can use this 
            <a href="https://openid.radamanthys.de/">site</a>, but this OpenID
            not reliable for general use.
            </p>
         
            Alternately, click your account provider:
         
            Article <b>%(title)s</b> from proposal <a href="/proposals/view/%(url_prop_title)s">%(prop_title)s</a>.<br/>
            Last changed on %(date)s.
         
            Article <b>%(title)s</b><br/>
            Last changed on %(date)s.
         
            History of article <b>%(article_title)s</b> in proposal <b>%(proposal_title)s</b>.
             
            This proposal has multiple heads which must first be merged
            before further editing can be done.
         
        Delete "%(a_title)s" from proposal %(p_title)s at revision %(rev)s
     
        Editing "%(a_title)s" from proposal %(p_title)s at revision %(rev)s
     
        Merge "%(article)s" from proposal %(proposal_title)s
     
        Rename "%(a_title)s" from proposal %(p_title)s at revision %(rev)s
      
            Articles in proposal <a href="/proposals/view/%(url_prop_title)s">%(prop_title)s</a>.
         Accept Accepted Added After:  Approval Archive Archived Article Articles Articles in trunk. Author Before:  Change Conflicting Articles Create Proposal Create a new article, to be applied with this proposal. Create a new article. Create a new proposal. Date Delete Difference between Proposal and Trunk. Edit Editing Email me when a proposal enters into the voting period. English German History History of Trunk. Log in Log in with OpenID Log in with your username and password: Login Logout Modified New Article No changes made. OpenID: Or use your OpenID: Parents Participated Password: Profile Proposal Proposals Reject Removed Rename Review Revive Save Sign in State Submit Summary Time Remaining Trunk Username: Voting Voting Mail enter your openid next previous Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-12-03 21:33+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
                <a href="/changes/from/%(url_title)s/view/%(url_rev)s">
                    %(date)s Änderung %(rev)s erstellt von %(user)s
                </a>
             
                    Wenn sie sich registrieren möchten oder bereits
                    registriert sind, geben Sie ihre OpenID ein
                 
                %(article_title)s aus dem Entwurf 
            <a href="/proposals/view/%(url_prop_title)s">%(prop_title)s</a> 
         
            <a href="/changes/view/%(url_rev)s">
                %(date)s Änderung %(rev)s erstellt von %(user)s
            </a>
             
                Historie von Article <b>%(article_title)s</b> im Stamm.
             
                Historie von Entwurf <b>%(proposal_title)s</b>.
             
                Zuletzt geändert am %(date)s.
             
                Dieser Entwurf hat mehrere aktuelle Revisionen! Konflikte
                im Artikel müssen behoben werden damit er weiter                 bearbeitet werden kann.
             
            <p><b>OpenID Info</b><br/>
            OpenID erlaubt ihnen, sich mit einem Benutzernamen auf  verschiedene Webseiten einzuloggen. Ihr Online Erlebniss wird damit erheblich vereinfacht.
            </p>

            <p>Sie können einen beliebigen OpenID Provider verwenden.
            Wenn Sie noch keine OpenID haben, können sie sich von 
            <a href="http://openid.net/get/">dieser Liste</a>            einen aussuchen.</p>
         
            Sie können alternativ auch auf ihren OpenID Provider clicken:
         
            Article <b>%(title)s</b> aus dem Entwurf <a href="/proposals/view/%(url_prop_title)s">%(prop_title)s</a>.<br/>
            Zuletzt geändert am %(date)s.
         
            Article <b>%(title)s</b><br/>
            Zuletzt geändert am %(date)s.
         
            Historie vom Article <b>%(article_title)s</b> im Entwurf <b>%(proposal_title)s</b>.
             
                Dieser Entwurf hat mehrere aktuelle Revisionen. Damit der
                Entwurf weiter bearbeitet werden kann müssen diese 
                zusammengeführt werden.
             
        Entferne "%(a_title)s" aus dem Entwurf %(p_title)s bei Revision %(rev)s
     
        Bearbeitung von "%(a_title)s" im Entwurf %(p_title)s bei Revision %(rev)s
     
        "%(article)s" im Entwurf %(proposal_title)s zusammenführen
     
        "%(a_title)s" aus dem Entwurf %(p_title)s bei Revision %(rev)s umbenennen
          
            Artikel im Entwurf <a href="/proposals/view/%(url_prop_title)s">%(prop_title)s</a>.
         Akzeptieren Angenommen Hinzugefügt Nachher:  Zustimmung Archivieren Archiviert Artikel Artikel Liste Artikel im Stamm Author Vorher:  Änderung Artikel mit Konflikte Neuer Entwurf Neuen Artikel erstellen, der mit diesem Entwurf angewandt wird Neuer Artikel Neuen Entwurf erstellen. Datum Löschen Unterschiede zwischen Entwurf und Stamm. Bearbeiten Bearbeitung Email am mich senden, wenn über einen Entwurf abgestimmt wird. Englisch Deutsch Historie Historie vom Stamm Einloggen Mit OpenID einloggen Mit Benutzername und Passwort anmelden: Einloggen Ausloggen Geändert Neuer Artikel Keine Änderungen OpenID: OpenID benutzen: Elternknoten Teilgenommen Passwort Konto Entwurf Entwürfe Ablehnen Entfernt Umbenennen Prüfung Wiederherstellen Speichern Einloggen Status Abschicken Zusammenfassung Verbleibende Zeit Stamm Benutzername: Abstimmung Mail über Abstimmung geben Sie ihre openid ein weiter zurück 