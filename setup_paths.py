import os
from socket import gethostname

BASEPATH = os.path.abspath("..")
HOSTNAME = gethostname()

def file_replace(filename, search, replace):
    file = open(filename + ".template")
    content = file.read()
    file = open(filename, 'w')
    file.write(content.replace(search, replace))
    file.close()
	
file_replace("apache/django.wsgi", "HOSTNAME", HOSTNAME)
file_replace("apache/django_wsgi.conf", "HOSTNAME", HOSTNAME)
file_replace("apache/httpd.conf", "HOSTNAME", HOSTNAME)

file_replace("apache/django.wsgi", "BASEPATH", BASEPATH)
file_replace("apache/django_wsgi.conf", "BASEPATH", BASEPATH)
file_replace("apache/httpd.conf", "BASEPATH", BASEPATH)

