from django.conf.urls.defaults import *
from django.views.generic.simple import redirect_to
from django.contrib import admin
from django_openid.registration import RegistrationConsumer
from articles.views import starting_page
import settings


admin.autodiscover()


urlpatterns = patterns('',
    (r'^$', starting_page),
    (r'^articles/', include('articles.urls')),
    (r'^proposals/', include('proposals.urls')),
    (r'^changes/', include('changes.urls')),
    (r'^comments/', include('comments.urls')),
    (r'^profiles/', include('profiles.urls')),
    (r'^openid/(.*)', RegistrationConsumer()),
    (r'^admin/(.*)', admin.site.root),
)


if settings.DEBUG:
    urlpatterns += patterns('', 
        (r'^static_media/(?P<path>.*)$', 'django.views.static.serve', 
                    {'document_root': settings.MEDIA_ROOT }),)


