#!/usr/bin/env python 
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import sys
import imp
import datetime
from time import sleep, time
from django.core.management import setup_environ
import settings

setup_environ(settings) 

import proposals.control as pctrl
from django.contrib.auth.models import User
from proposals.models import Proposal, Vote
from dvcs.common import MergeStrategy
from common import community_settings


REFRESH_TIME = 60.0 # in seconds
VOTING, REVIEW, EDITING, ACCEPTED, ARCHIVED = Proposal.State.VALUES
now = datetime.datetime.now
get_props = Proposal.objects.filter
get_votes = Vote.objects.filter


class CheckProposal(object):
    
    def __init__(self, proposal):
        self.proposal = proposal
        self()

    def __call__(self):
        if self.proposal.state is REVIEW:
            self.check_review()
        elif self.proposal.state is VOTING:
            self.check_voting()

    def duration_over(self, start, duration):
        return now() >= (start + datetime.timedelta(hours=duration))
    
    def check_review(self):
        duration = community_settings.review_duration()
        if self.duration_over(self.proposal.review_start, duration):
            pctrl.start_voting(self.proposal)
            print "%s REVIEW OVER: %s" % (now(), self.proposal)
    
    def is_accepted(self):
        min_value = community_settings.minimum_approval()
        votes = get_votes(proposal=self.proposal).count()
        min_approval = min_value * 0.01 * votes
        approvals = get_votes(proposal=self.proposal, accepted=True).count()
        return approvals >= min_approval and self.enough_votes()

    def enough_votes(self):
        min_value = community_settings.minimum_participation()
        users = User.objects.all().count()
        min_participation = min_value * 0.01 * users
        votes = get_votes(proposal=self.proposal).count()
        return votes >= min_participation

    def can_evaluate(self):
        duration = community_settings.voting_duration()
        start = self.proposal.voting_start
        return (duration and self.duration_over(start, duration) or 
                not duration and self.enough_votes())

    def check_voting(self):
        if self.can_evaluate():
            if self.is_accepted():
                pctrl.accept(self.proposal)
                main_repo = settings.DVCS.Repo(settings.MAIN_REPO)
                main_repo.pull(self.proposal.path, MergeStrategy.CAUTIOUS)
                self.propagate_changes()
                print '%s ACCEPTED PROPOSAL: %s' % (now(), self.proposal)
            else:
                pctrl.reject(self.proposal)
                print '%s REJECTED PROPOSAL: %s' % (now(), self.proposal)

    def propagate_changes(self):
        self.update(VOTING, MergeStrategy.CAUTIOUS) 
        self.update(REVIEW, MergeStrategy.CAUTIOUS) 
        self.update(EDITING, MergeStrategy.OPTIMISTIC) 
        self.update(ARCHIVED, MergeStrategy.OPTIMISTIC) 

    def update(self, state, strategy):
        for proposal in get_props(state=state):
            repo = settings.DVCS.Repo(proposal.path) 
            repo.pull(settings.MAIN_REPO, strategy)
            print '%s UPDATED PROPOSAL: %s' % (now(), proposal.title)


def main():
    print "running ..."
    while True:
        beginn = time()
        map(CheckProposal, get_props(state=REVIEW))
        map(CheckProposal, get_props(state=VOTING))
        passed = time() - beginn
        sleep(REFRESH_TIME - passed)
   

if __name__ == '__main__':
    main()


