# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 Fabian Barkhau <fabian.barkhau@gmail.com>
# Copyright (C) 2009 Manuel Barkhau <mbarkhau@gmail.com>
# Copyright (C) 2009 Christian Hahn <ch@radamanthys.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import dvcs.hg as mercurial 

#dummy ugettext replacement to prevent circular imports
#from django.utils.translation import ugettext as _
_ = lambda s : s

DEBUG = True
TEMPLATE_DEBUG = DEBUG
PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))

ADMINS = (
    ('fabe', 'fabian.barkhau@googlemail.com'),
    ('manu', 'mbarkhau@googlemail.com'),
)

MANAGERS = ADMINS

# ===== BEGIN Database =====

try:
    # try custom db_settings.py
    from custom_settings import *
except ImportError, e:
    # Fallback to std if no custom exists
    from std_custom_settings import *

# ===== END Database =====

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Europe/Berlin'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

LANGUAGES = (
    ('de', _("German")),
    ('en', _("English")),
)

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

SITE_ID = 1

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media').replace('\\','/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static_media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '7e+3-@z85^fx310ja)w)9&%scpodm5-kt-ozcn8mj)o$q&k#0g'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.request",
    "django.core.context_processors.media",
    "common.context_processors.current_url",
    "common.context_processors.context_class",
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'pagination.middleware.PaginationMiddleware',
    'django_openid.consumer.SessionConsumer',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Always use absolute paths with forward slashes.
    os.path.join(PROJECT_DIR, 'templates').replace('\\','/'),
)

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django_openid', 
    'south',
    'common',
    'articles',
    'proposals',
    'comments',
    'pagination',
    'profiles',
)

# ===== BEGIN candiwi =====

DVCS = mercurial
MAIN_REPO = os.path.join(os.path.dirname(PROJECT_DIR), 'candiwi_main')
CLONE_DIR = os.path.join(os.path.dirname(PROJECT_DIR), 'candiwi_props')

# ===== END candiwi =====


# ===== BEGIN openid =====

# https://launchpad.net/django-openid-auth
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
OPENID_CREATE_USERS = True
OPENID_UPDATE_DETAILS_FROM_SREG = True
LOGIN_URL = '/openid/login'
AUTH_PROFILE_MODULE = 'profiles.UserProfile'
# OPENID_SSO_SERVER_URL = 'https://login.launchpad.net/'

# ===== END openid =====


